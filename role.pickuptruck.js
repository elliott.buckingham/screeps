module.exports = {
    assignSource:function(creep) {
        for (let cName in Game.creeps) {
            var c = Game.creeps[cName];
            if (c.memory.role === "miner"){
                let sourcetomove = c.memory.source
                for (let mName in Game.creeps) {
                    var m = Game.creeps[mName];
                    if (m.memory.role === "pickup" && m.memory.source === undefined){
                        m.memory.source = sourcetomove
                        break;
                    }
                    else {
                        //already specified
                    }

                    
                }
            }
        }
    },

    run:function(creep) {
        var pickuploc = Game.getObjectById(creep.memory.source)
        if (creep.store.getFreeCapacity() != 0) {
            creep.moveTo(pickuploc), {visualizePathStyle: {stroke: '#ffaa00'}};
            let target = creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES);
            if (creep.pickup(target) == ERR_NOT_IN_RANGE) {
                creep.moveTo(pickuploc), {visualizePathStyle: {stroke: '#ffaa00'}};
            }
        }
        else {
            var targets = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_EXTENSION || structure.structureType == STRUCTURE_SPAWN) &&
                        structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
                }
            });
            if(targets.length > 0) {
                if(creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#ffffff'}});
                }
            }
        }
        
    }




};