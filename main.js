var roleMiner = require('role.miner');
var rolePickupTruck = require('role.pickuptruck');

module.exports.loop = function () {


    //count the number of creeps with different tasks
    var numberOfMiners = _.sum(Game.creeps, (c) => c.memory.role == 'miner');
    var numberOfPickups = _.sum(Game.creeps, (c) => c.memory.role == 'pickup');
    
    let minMinerNu = 2
    let minPickups = 2



    //checking number of miners to pickups
    if (numberOfMiners > minPickups){
        let minPickups = numberOfMiners;
    }
    


    // check for memory entries of died creeps by iterating over Memory.creeps
    for (let name in Memory.creeps) {
        // and checking if the creep is still alive
        if (Game.creeps[name] == undefined) {
            // if not, delete the memory entry
            delete Memory.creeps[name];
            console.log("Memory cleared")
        }
    }

    for (let name in Game.creeps) {
        //Get the Creep object
        var creep = Game.creeps[name];

        //if creep is a miner
        if (creep.memory.role == 'miner') {
            if (typeof creep.memory.source === 'undefined') {
                roleMiner.assignSource(creep);
            }
            roleMiner.run(creep);
            //console.log("Miner role Called")
        }

        //if creep is a pickup
        if (creep.memory.role == 'pickup') {
            if (typeof creep.memory.source === 'undefined') {
                rolePickupTruck.assignSource(creep);
            }
            rolePickupTruck.run(creep);
            //console.log("Miner role Called")
        }


    }

    //count the number of creeps with different tasks
    var numberOfMiners = _.sum(Game.creeps, (c) => c.memory.role == 'miner');
    var numberOfPickups = _.sum(Game.creeps, (c) => c.memory.role == 'pickup');


    var name = undefined;

    //if there are not enough miners
    if (numberOfMiners < minMinerNu) {
        //console.log("Create miner func called") //USED FOR DEBUG
        //try to Spawn one{
        var newName = 'Miner' + Game.time;
        name = Game.spawns['Spawn1'].spawnCreep([WORK, WORK, MOVE], newName, {
            memory: {role: 'miner'}
        });
        //console.log(name)
    }

    if (numberOfPickups < minPickups && numberOfMiners > numberOfPickups) {
        //console.log("Create pickup func called") //USED FOR DEBUG
        //try to Spawn one
        var newName = 'Pickup' + Game.time;
        name = Game.spawns['Spawn1'].spawnCreep([CARRY, CARRY, MOVE], newName, {
            memory: {role: 'pickup'}
        });
        //console.log(name)
    }

    if(Game.spawns['Spawn1'].spawning) {
        var spawningCreep = Game.creeps[Game.spawns['Spawn1'].spawning.name];
        Game.spawns['Spawn1'].room.visual.text(
            '🛠️' + spawningCreep.memory.role,
            Game.spawns['Spawn1'].pos.x + 1,
            Game.spawns['Spawn1'].pos.y,
            {align: 'left', opacity: 0.8});
    }



};